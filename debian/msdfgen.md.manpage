% msdfgen(1) | User Commands
%
% "May 27 2023"

# NAME

msdfgen - generate signed distance fields

# SYNOPSIS

**msdfgen** [MODE] INPUT [OPTION]...

# DESCRIPTION
This manual page documents briefly the **msdfgen** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**msdfgen** is a program that generates signed distance fields from input
files such as fonts, vector graphics in SVG format, or text-encoded shape
files. It can generate a variety of distance field types and has a number
of options affecting the output format or how the input is interpreteted.

# OPTIONS
The program options are broken into three main parts - the mode of operation,
the input type, and modifier options to the mode of operation. Each is
described below.

## MODES
The mode can be one of the following.

**sdf**
:   Generate a conventional monochrome signed distance field.

**psdf**
:   Generate a monchrome signed pseudo-distance field.

**msdf** (default)
:   Generate a multi-channel signed distance field.

**mtsdf**
:   Generate a combined multi-channel and true signed distance field in the
:   alpha channel.

**metrics**
:   Report shape metrics only.

## INPUT
The input can be specified as one of the following. See
[Shape Description](#shape-description) for an explanation of the text shape
definition format.

**-defineshape** DEFINITION
:   Defines input shape using the ad-hoc text definition format.

**-font** FILENAME CHARACTER_CODE
:   Loads a single glyph from the specified font file. The format of the
:   character code is '?' (ASCII character), 63 (decimal value), 0x3F (Unicode
:   hexadecimal), or g34 (glyph index).

**-shapedesc** FILENAME
:   Loads a text description of the shape from a file.

**-stdin**
:   Reads the text shape description from the standard input.

**-svg** FILENAME
:   Loads an SVG file. Note that only the last vector path in the file will
:   be used.

**-varfont** FILENAME_AND_VARIABLES CHARACTER_CODE
:   Loads a single glyph from a variable font. Specify variables as
:   _x.ttf?var1=0.5&var2=1_

## FLAGS
**-angle** ANGLE
:   Specifies the minimum angle between adjacent edges to be considered a
:   corner. Append D for degrees.

**-ascale** X_SCALE Y_SCALE
:   Sets the scale used to convert shape units to pixels asymmetrically.

**-autoframe**
:   Automatically scales (unless specified) and translates the shape to fit.

**-coloringstrategy** _simple_ | _inktrap_ | _distance_
:   Selects the strategy of the edge coloring heuristic.

**-distanceshift SHIFT
:   Shifts all normalized distances in the output distance field by this value.

**-edgecolors SEQUENCE
:   Overrides automatic edge coloring with the specified color sequence.

**-errorcorrection** MODE
:   Changes the MSDF/MTSDF error correction mode. See
:   [Error Correction Modes](#error-correction-modes) for a list of valid modes.

**-errordeviationratio** RATIO
:   Sets the minimum ratio between the actual and maximum expected distance
:   delta to be considered an error.

**-errorimproveratio** RATIO
:   Sets the minimum ratio between the pre-correction distance error and the
:   post-correction distance error.

**-estimateerror**
:   Computes and prints the distance field's estimated fill error to the
:   standard output.

**-exportshape** FILENAME
:   Saves the shape description into a text file that can be edited and loaded
:   using -shapedesc.

**-fillrule** _nonzero_ | _evenodd_ | _positive_  | _negative_
:   Sets the fill rule for the scanline pass. Default is nonzero.

**-format** _png_ | _bmp_ | _tiff_ | _text_ | _textfloat_ | _bin_ | _binfloat_ | _binfloatbe_
:   Specifies the output format of the distance field. Otherwise it is chosen
:   based on output file extension.

**-guessorder**
:   Attempts to detect if shape contours have the wrong winding and generates
:   the SDF with the right one.

**-help**
:   Displays this help.

**-legacy**
:   Uses the original (legacy) distance field algorithms.

**-nooverlap**
:   Disables resolution of overlapping contours.

**-noscanline**
:   Disables the scanline pass, which corrects the distance field's signs
:   according to the selected fill rule.

**-o** FILENAME
:   Sets the output file name. The default value is "output.png".

**-printmetrics**
:   Prints relevant metrics of the shape to the standard output.

**-pxrange** RANGE
:   Sets the width of the range between the lowest and highest signed distance
:   in pixels.

**-range** RANGE
:   Sets the width of the range between the lowest and highest signed distance
:   in shape units.

**-reverseorder**
:   Generates the distance field as if the shape's vertices were in reverse
:   order.

**-scale** SCALE
:   Sets the scale used to convert shape units to pixels.

**-seed** N
:   Sets the random seed for edge coloring heuristic.

**-size** WIDTH HEIGHT
:   Sets the dimensions of the output image.

**-stdout**
:   Prints the output instead of storing it in a file. Only text formats are
:   supported.

**-testrender** FILENAME WIDTH HEIGHT
:   Renders an image preview using the generated distance field and saves it as
:   a PNG file.

**-testrendermulti** FILENAME WIDTH HEIGHT
:   Renders an image preview without flattening the color channels.

**-translate** X Y
:   Sets the translation of the shape in shape units.

**-windingpreprocess**
:   Attempts to fix only the contour windings assuming no self-intersections and
:   even-odd fill rule.

**-yflip**
:   Inverts the Y axis in the output distance field. The default order is bottom
:   to top.

# ERROR CORRECTION MODES
_auto-fast_
:   Detects inversion artifacts and distance errors that do not affect edges by
:   range testing.

_auto-full_
:   Detects inversion artifacts and distance errors that do not affect edges by
:   exact distance evaluation.

_auto-mixed_ (default)
:   Detects inversions by distance evaluation and distance errors that do not
:   affect edges by range testing.

_disabled_
:   Disables error correction.

_distance-fast_
:   Detects distance errors by range testing. Does not care if edges and corners
:   are affected.

_distance-full_
:   Detects distance errors by exact distance evaluation. Does not care if edges
:   and corners are affected, slow.

_edge-fast_
:   Detects inversion artifacts only by range testing.

_edge-full_
:   Detects inversion artifacts only by exact distance evaluation.

_help_
:   Displays available error correction modes.

# SHAPE DESCRIPTION SYNTAX
The text shape description has the following syntax.

 * Each closed contour is enclosed by braces: `{ <contour 1> } { <contour 2> }`
 * Each point (and control point) is written as two real numbers separated by a
   comma.
 * Points in a contour are separated with semicolons.
 * The last point of each contour must be equal to the first, or the symbol `#`
   can be used, which represents the first point.
 * There can be an edge segment specification between any two points, also
   separated by semicolons.
   This can include the edge's color (`c`, `m`, `y` or `w`) and/or one or two
   Bézier curve control points inside parentheses.

For example,
```
{ -1, -1; m; -1, +1; y; +1, +1; m; +1, -1; y; # }
```
would represent a square with magenta and yellow edges,
```
{ 0, 1; (+1.6, -0.8; -1.6, -0.8); # }
```
is a teardrop shape formed by a single cubic Bézier curve.

